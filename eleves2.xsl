<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:fo="http://www.w3.org/1999/XSL/Format"
version="1.0" >
<xsl:key name="eleve-by-ville" match="eleve" use="ville"/>
<xsl:key name="eleve-by-lycee" match="eleve" use="lycee"/>

<xsl:template match="/eleves">

	
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">

	  <fo:layout-master-set>
	     <fo:simple-page-master master-name="couverture"
		page-height="29.7cm"
		page-width="21cm"
		margin-top="1cm"
		margin-bottom="1cm"
		margin-left="1.5cm"
		margin-right="0.5cm">
		<fo:region-body margin-top="6cm" margin-bottom="6cm"/> <!-- obligatoire !!! -->
		<fo:region-after extent="3cm"/>
	     </fo:simple-page-master>
	    
	     <fo:simple-page-master master-name="ville"
		page-height="29.7cm"
		page-width="21cm"
		margin-top="1cm"
		margin-bottom="1cm"
		margin-left="1.5cm"
		margin-right="0.5cm">
	      <fo:region-body margin-top="2cm" margin-bottom="2cm"/>
	      <!-- doivent être supérieures aux "extent" des régions correspondantes -->
	      <fo:region-before extent="2cm"/>
	      <fo:region-after extent="1.5cm"/>
	     </fo:simple-page-master>
	  </fo:layout-master-set>
	 
	  <fo:page-sequence master-reference="couverture">
		  <fo:static-content flow-name="xsl-region-after"> <!-- idem -->
	      <fo:block text-align="center">
	      Bussell Xavier and Campart Clémentine
	      </fo:block>
	    </fo:static-content>
	      <fo:flow flow-name="xsl-region-body">
		<fo:block text-align="center" font-size="14pt" font-style="italic" space-after="16pt">
		 Micro-projet, Technologies web avancées
		</fo:block>
		<fo:block text-align="center" font-size="18pt" font-weight="bold">
		 Compte-rendu des inscriptions des élèves aux spécialités
		</fo:block>
		<fo:block text-align="center">
		<fo:external-graphic src="url('img/image.jpeg')"></fo:external-graphic>
		</fo:block>

	      </fo:flow>
	  </fo:page-sequence>
	   <fo:page-sequence master-reference="couverture">
	      <fo:flow flow-name="xsl-region-body">
		<fo:block text-align="center" font-size="14pt" font-style="italic" space-after="16pt">
		</fo:block>
	      </fo:flow>
	  </fo:page-sequence>


	  <fo:page-sequence master-reference="ville">
	 
	    <fo:static-content flow-name="xsl-region-before"> <!-- attention ! pas "flow" -->
	      <fo:block text-align="center" font-size="8pt" font-style="italic">
	      La liste des lycées par ville
	      </fo:block>
	    </fo:static-content>
	   
	    <fo:static-content flow-name="xsl-region-after"> <!-- idem -->
	      <fo:block text-align="center">
	      p. <fo:page-number />
	      </fo:block>
	    </fo:static-content>
	  	 

	    <fo:flow flow-name="xsl-region-body"> <!-- un seul "flow" !!!  -->
	      
		<xsl:for-each select="eleve[count(. | key('eleve-by-ville', ville)[1]) = 1]">
			<xsl:sort lang="FR" select="ville"/>
			
			<fo:block text-indent="2em" font-size="14pt" text-align="center" font-weight="bold">
				   <xsl:value-of select="ville"/> 
				</fo:block>
			<xsl:for-each select="key('eleve-by-ville', ville)[not(lycee = preceding::eleve/lycee)]">
					<xsl:sort lang="FR" select="lycee"/>
					
						<fo:block text-indent="2em" font-size="9pt" text-align="center" font-weight="normal"><xsl:value-of select="lycee"/> </fo:block>
			</xsl:for-each>
		</xsl:for-each>
	

	    </fo:flow>
	  </fo:page-sequence>
	  
	  <fo:page-sequence master-reference="ville">
         <fo:static-content flow-name="xsl-region-before"> <!-- attention ! pas "flow" -->
	      <fo:block text-align="center" font-size="8pt" font-style="italic">
	      Liste par lycée
	      </fo:block>
	     </fo:static-content>
	   
	     <fo:static-content flow-name="xsl-region-after"> <!-- idem -->
	      <fo:block text-align="center">
	      p. <fo:page-number />
	      </fo:block>
	     </fo:static-content>
	     
	     
	     <fo:flow flow-name="xsl-region-body"> <!-- un seul "flow" !!!  -->
	     
	     <xsl:for-each select="eleve[count(. | key('eleve-by-lycee', lycee)[1]) = 1]">
			<xsl:sort select="lycee"/>
			<fo:block break-before="page"></fo:block>
			    <fo:block text-indent="2em" font-size="14pt" text-align="center" font-weight="bold" space-after="20pt">
				   <xsl:value-of select="lycee"/>
				</fo:block>
				<xsl:variable name="unlycee" select="key('eleve-by-lycee', lycee)"/>
				<fo:block text-indent="2em" font-size="14pt" text-align="center" font-weight="bold" space-after="20pt">
					Nombre de F : <xsl:value-of select="count($unlycee[genre='F'])"/>
				</fo:block>
				<fo:block text-indent="2em" font-size="14pt" text-align="center" font-weight="bold" space-after="20pt">
					Nombre de M : <xsl:value-of select="count($unlycee[genre='M'])"/>
				</fo:block>
				
				
				<xsl:for-each select="key('eleve-by-lycee', lycee)">
					<xsl:sort select="specialite"/>
					
					
					
					
					<fo:table table-layout="fixed" border-bottom="1px solid #ccc" font-size=".9em" color="#666">
						
					<fo:table-column column-width="12cm" />
					<fo:table-column column-width="3.5cm" />
					<fo:table-column column-width="3.5cm" />
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell>
								<fo:block><xsl:value-of select="specialite"/></fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block text-align="center" font-weight="bold"><xsl:value-of select="nom"/></fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block text-align="right"><xsl:value-of select="prenom"/></fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				    </fo:table>
				    
				    
				     
				</xsl:for-each>
		</xsl:for-each>
		
	     
	     
	     
			 
		 </fo:flow>
		 
		 
		  
	  </fo:page-sequence>
		   

	      

	</fo:root>
</xsl:template>
</xsl:stylesheet>
