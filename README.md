# Micro-Projet : Compte-rendu des inscription des élèves aux spécialités

Le fichier en entrée au format XML : `query_file/eleves.xml`.

## 1. XSD

Vérification de la conformité au schéma `eleves.xsd` :
```
xmllint --schema eleves.xsd query_file/eleves.xml --noout
```

## 2. XSLT et HTML/CSS

Génréation du fichier **HTML** à partir de `eleves.xsl` :
```
xsltproc eleves.xsl query_file/eleves.xml > eleves.html
```

## XSL-FO et PDF

Génréation du fichier **HTML** à partir de `eleves2.xsl` :
```
fop -xsl eleves2.xsl -xml query_file/eleves.xml -pdf eleves.pdf
```
