<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:output method="html"/>

	<xsl:key name="eleve-by-ville" match="eleve" use="ville"/>
	<xsl:key name="eleve-by-lycee" match="eleve" use="lycee"/>

	<xsl:template match="/eleves">
	<html>
		<body>
			<h2>Liste des lycées par ville</h2>
			<xsl:call-template name="lycee"/>

			<h2>Liste des lycées et des élèves triés par sécialité</h2>
			<xsl:call-template name="liste_eleve"/>
		</body>
	</html>
	</xsl:template>

	<xsl:template name="lycee">
		<xsl:for-each select="eleve[count(. | key('eleve-by-ville', ville)[1]) = 1]">
			<xsl:sort lang="FR" select="ville"/>
			<h3><xsl:value-of select="ville"/></h3>
			<ul>
				<xsl:for-each select="key('eleve-by-ville', ville)[not(lycee = preceding::eleve/lycee)]">
					<xsl:sort lang="FR" select="lycee"/>
						<li><a href="#L{generate-id()}"><xsl:value-of select="lycee"/></a></li>
				</xsl:for-each>
			</ul>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="liste_eleve">
		<xsl:for-each select="eleve[count(. | key('eleve-by-lycee', lycee)[1]) = 1]">
			<xsl:sort select="lycee"/>
			<h3 id="L{generate-id()}"><xsl:value-of select="lycee"/></h3>
			<ul>
				<xsl:for-each select="key('eleve-by-lycee', lycee)">
					<xsl:sort select="specialite"/>
					<li>
						<xsl:value-of select="nom"/><xsl:text>, </xsl:text><xsl:value-of select="prenom"/>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>
